import java.util.Scanner;

public class Atm {
    int payment;


    public void setNumber(String cardNumber) {
        System.out.println("Numer karty to: "+ cardNumber );
        System.out.println(".............");
        System.out.println("...Loading...");
        System.out.println(".............");
    }


    public boolean checkPassword(String accountPassword, String checkedPassword) {
       // boolean accept = accountPassword.equals(checkedPassword);
        System.out.println();

        return accountPassword.equals(checkedPassword);

    }

    public void setChoose(int menu, int amountOfMoney) {

        switch (menu) {
            case AtmActions.STATUS:
                System.out.println("Stan Twojego konta to: "  + amountOfMoney + " PLN");
                break;
            case AtmActions.WITHDRAW:

                Scanner drawMoney = new Scanner(System.in);

                do {
                    System.out.println("Podaj ile chcesz wypłacić: ");

                    payment = drawMoney.nextInt();
                    if (payment < 0) {
                        System.out.println("To nie jest opcja wpłatomatu! \nWartość musi być dodatnia");
                    }

                } while (payment <= 0);

                int newAmount = amountOfMoney - payment;
                System.out.println("Stan konta teraz to " + newAmount + "PLN");
                break;

            case AtmActions.REMITTANCE:
                Scanner remittance = new Scanner(System.in);

                do {
                    System.out.println("Włóż banknoty: ");

                    payment = remittance.nextInt();
                    if (payment < 0) {
                        System.out.println("To jest wpłatomat! \nWartość musi być dodatnia");
                    }

                } while (payment <= 0);

                newAmount = amountOfMoney + payment;
                System.out.println("Stan konta teraz to " + newAmount + "PLN");
                break;

            case AtmActions.FINISH:
                System.out.println("Dziękujemy za skorzystanie z usług.");
                System.exit(0);
        }
    }
    public int withdraw() {
        return payment;
    }
}
