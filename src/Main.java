import java.util.Scanner;

public class Main {

        public static void main(String[] args) {
            Atm b = new Atm();
            ClientAccounts ca = new ClientAccounts();

            int choose;

            Account piotrek = new Account("piotrek",3500, "fejcher");
            Account ania = new Account("ania", 10000, "javorska");
            Account tomek = new Account("tomek", -100, "ziolek");



        System.out.println("BANKOMAT");
        System.out.println(" ");
        System.out.println("Włóż kartę ");
        System.out.println("Podaj numer karty (imię): ");

            Scanner number = new Scanner(System.in);
            String ownerName = number.nextLine();
            b.setNumber(ownerName);

            ca.checkLogin(ownerName);

                System.out.println("Podaj hasło: ");
                Scanner pass = new Scanner(System.in);
                String password = pass.nextLine();


                switch (ownerName) {
                    case "piotrek":

                        if (b.checkPassword(piotrek.getPassword(), password)) {
                            break;

                        } else {

                                System.out.println("Podano nieprawidłowe hasło! ");
                                System.exit(0);

                        }
                    case "ania":

                        if (b.checkPassword(ania.getPassword(), password)) {
                            break;

                        } else {

                                System.out.println("Podano nieprawidłowe hasło! ");
                                System.exit(0);
                        }
                    case "tomek":

                        if (b.checkPassword(tomek.getPassword(), password)) {
                            break;

                        } else {

                                System.out.println("Podano nieprawidłowe hasło! ");
                                System.exit(0);

                        }
                        default: {
                            System.out.println("Konta nie ma w systemie.");
                            System.exit(0);
                            }
                }

            do {
                System.out.println(" ");
                System.out.println("Stan konta      - wybierz 1 \nWypłata         - wybierz 2 \nWpłać pieniądze - wybierz 3 \nZakończ         - wybierz 0");
                Scanner clientChoose = new Scanner(System.in);
                choose = clientChoose.nextInt();
                if (choose == AtmActions.FINISH || choose == AtmActions.STATUS || choose == AtmActions.WITHDRAW || choose == AtmActions.REMITTANCE) {

                    if(ownerName.equals(piotrek.getCardNumber()) || ownerName.equals(ania.getCardNumber()) || ownerName.equals(tomek.getCardNumber())) {
                        switch (ownerName) {
                            case "piotrek":
                                b.setChoose(choose, piotrek.getMoneyInAccount());
                                if (choose==AtmActions.WITHDRAW) {
                                    piotrek.moneyWithdraw(b.withdraw());
                                }
                                else if (choose==AtmActions.REMITTANCE) {
                                    piotrek.moneyRemittance((b.withdraw()));
                                }
                                break;

                            case "ania":
                                b.setChoose(choose, ania.getMoneyInAccount());
                                if (choose==AtmActions.WITHDRAW) {
                                    ania.moneyWithdraw(b.withdraw());
                                }
                                else if (choose==AtmActions.REMITTANCE) {
                                    ania.moneyRemittance((b.withdraw()));
                                }
                                break;

                            case "tomek":
                                b.setChoose(choose, tomek.getMoneyInAccount());
                                if (choose==AtmActions.WITHDRAW) {
                                    tomek.moneyWithdraw(b.withdraw());
                                }
                                else if (choose==AtmActions.REMITTANCE) {
                                    tomek.moneyRemittance((b.withdraw()));
                                }
                                break;
                        }

                    }

                    else {
                        System.out.println("Tego konta nie ma w systemie");
                        System.exit(0);
                    }

                } else
                    System.out.println("Niewłaściwy wybór.");
            }

            while (choose != AtmActions.FINISH || choose != AtmActions.STATUS || choose != AtmActions.WITHDRAW || choose !=AtmActions.REMITTANCE);

    }
}


