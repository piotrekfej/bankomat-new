import java.util.*;

public class ClientAccounts {
    public static void main(String[] args) {

        Account piotrek = new Account("piotrek",3500, "fejcher");
        Account ania = new Account("ania", 10000, "javorska");
        Account tomek = new Account("tomek", -100, "ziolek");



        ArrayList<String> accountsList = new ArrayList<>();
        accountsList.add(piotrek.getCardNumber());
        accountsList.add(ania.getCardNumber());
        accountsList.add(tomek.getCardNumber());


        LinkedHashMap<String, String> accounts = new LinkedHashMap<>();
        accounts.put(tomek.getCardNumber(),tomek.getPassword());
        accounts.put(piotrek.getCardNumber(),piotrek.getPassword());
        accounts.put(ania.getCardNumber(),ania.getPassword());

        // System.out.println(accounts);

        System.out.println("Podaj login: ");
        Scanner lg = new Scanner(System.in);
        String login = lg.nextLine();

        if (accountsList.contains(login))
        {
            System.out.println("Login znaleziono w bazie danych");
        }
        else
        {
            System.out.println("Login nieznany");
            System.exit(0);
        }


        System.out.println("Podaj hasło: ");
        Scanner ps = new Scanner(System.in);
        String password = ps.nextLine();

        if (password.equals(accounts.get(login)))
        {
            System.out.println("Zalogowano do konta");
        }
        else
        {
            System.out.println("Niewłaściwe hasło");
            System.exit(0);
        }


    }
    public void checkLogin(String podaneHaslo)
    {
        System.out.println("Podałeś login: " + podaneHaslo);
    }
}
